package com.example.worldcinema.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.worldcinema.databinding.ActivityRegistrationBinding
import com.example.worldcinema.singuppscreen.SingAppScreenActivity

open class RegistrationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegistrationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRegistrationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.accButton.setOnClickListener {
            startActivity(
                Intent(this, SingAppScreenActivity::class.java)
            )
        }
        binding.regestrationButton.setOnClickListener {
            startActivity(
                Intent(this, SingAppScreenActivity::class.java)
            )
        }

    }
}