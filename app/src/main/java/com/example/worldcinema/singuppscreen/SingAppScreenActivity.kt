package com.example.worldcinema.singuppscreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.worldcinema.databinding.ActivitySingAppScreenBinding
import com.example.worldcinema.main.MainActivity
import com.example.worldcinema.main.RegistrationActivity

class SingAppScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySingAppScreenBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySingAppScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.enterButton.setOnClickListener {
            val email = binding.emailText.text.toString()
            val password = binding.passwordText.text.toString()


            if (email.isEmpty())
                binding.emailText.error = "Введите имейл"
            else if (!email.contains("@") && !email.contains("."))
                binding.emailText.error = "Введите корректный иимейл"
            else if (password.isEmpty())
                binding.passwordText.error = "Введите пароль"
            else {
                startActivity(
                    Intent(this, MainActivity::class.java)
                )
                finish()
            }
        }

        binding.regestrationButton.setOnClickListener {
            startActivity(
                Intent(this, RegistrationActivity::class.java)
            )
        }


    }
}