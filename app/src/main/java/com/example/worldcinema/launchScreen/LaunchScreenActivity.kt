package com.example.worldcinema.launchScreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.example.worldcinema.databinding.ActivityLaunchScreenBinding
import com.example.worldcinema.singuppscreen.SingAppScreenActivity

class LaunchScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLaunchScreenBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLaunchScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(
                Intent(this, SingAppScreenActivity::class.java)
            )
            finish()

        }, 3000)
    }
}